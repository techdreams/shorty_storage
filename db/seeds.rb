# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
StoreHour.create(:day => "Monday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => false)
StoreHour.create(:day => "Tuesday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => true)
StoreHour.create(:day => "Wednesday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => true)
StoreHour.create(:day => "Thursday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => true)
StoreHour.create(:day => "Friday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => false)
StoreHour.create(:day => "Saturday", :opening_time => "10:00 am" , :closing_time => "4:00 am", :is_closed => true)
StoreHour.create(:day => "Sunday", :opening_time => "12:00 pm" , :closing_time => "4:00 am", :is_closed => false)

