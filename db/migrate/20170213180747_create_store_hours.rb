class CreateStoreHours < ActiveRecord::Migration
  def change
    create_table :store_hours do |t|
      t.string :day
      t.string :opening_time
      t.string :closing_time
      t.boolean :is_closed

      t.timestamps null: false
    end
  end
end
