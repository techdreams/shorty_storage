class StaticController < ApplicationController

  def about
  end

  def locations
  end

  def services
  end

  def resources
  end

  def contacts
    @contact = Contact.new
    @store_hours = StoreHour.limit(7)
  end

  def create_contacts
    post_params = params[:post]
    Contact.create(:name => post_params[:name], :email => post_params[:email], :phone => post_params[:phone], 
                   :message => post_params[:message])
    UserMailer.thanking_email_to_visitor(post_params[:email], post_params[:name]).deliver
    UserMailer.notify_email_to_admin(post_params[:email], post_params[:message]).deliver

    respond_to do |format|
      format.js
    end
  end


  def privacy_policy
  end
end
