class AdminController < ApplicationController

   before_action :authenticate_user!


	layout 'admin'

	def index
	end

	def store_timing
        @store_hours = StoreHour.limit(7)
	end

    def update_store_hours
        @store_hours = StoreHour.limit(7)
        @store_hours.each_with_index do |week_day, index|
        # byebug
        week_day.update_attributes(:opening_time => params[:"opening_time_#{index}"], :closing_time => params[:"closing_time_#{index}"], :is_closed => params[:"is_closed_#{index}"])
        end
        redirect_to (:back), notice: "Store Hours updated successfully"
    end

    def pricing
    end
    	
    def announcement
    end
end
