class UserMailer < ApplicationMailer
  # default from: 'notifications@example.com'
 
  def thanking_email_to_visitor(email, user_name)
  	@user_name = user_name
    @user_email = email
    mail(to: @user_email, subject: 'Thanks for visiting')
  end

  def notify_email_to_admin(email, message)
    @user_email = email
    @user_message = message
    mail(to: ENV['RECEIVER_EMAIL'], subject: 'A user sent a message')
  end

end
